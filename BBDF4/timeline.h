#pragma once
#include <string>
#include "Header1.h"

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
//using namespace std;
namespace BBDF4 {

	/// <summary>
	/// Summary for timeline
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class timeline : public System::Windows::Forms::Form
	{
	public:
		timeline(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~timeline()
		{
			if (components)
			{
				delete components;
			}
		}

	protected: 

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->SuspendLayout();
			// 
			// timeline
			// 
			this->AccessibleRole = System::Windows::Forms::AccessibleRole::Window;
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(1370, 750);
			this->Name = L"timeline";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"timeline";
			this->WindowState = System::Windows::Forms::FormWindowState::Maximized;
			this->Load += gcnew System::EventHandler(this, &timeline::timeline_Load);
			this->Activated += gcnew System::EventHandler(this, &timeline::timeline_Load);
			this->Enter += gcnew System::EventHandler(this, &timeline::timeline_Load);
			this->Click += gcnew System::EventHandler(this, &timeline::timeline_Load);
			this->Validated += gcnew System::EventHandler(this, &timeline::timeline_Load);
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void timeline_Load(System::Object^  sender, System::EventArgs^  e) {

			
			timeline::Show();
			int initWidth = 15;
			Graphics ^ formGraphics = this->CreateGraphics();
			formGraphics->Clear(Color::White);
			System::Drawing::Pen ^ myPen;
			myPen = gcnew System::Drawing::Pen(System::Drawing::Color::RoyalBlue);
            int i;
			
		array<String^>^ buffers = gcnew array<String^>(16) {
		"B1\nw", "B1\nr", "B2\nw",
        "B2\nr", "B8\nw", "B8\nr", "B3\nw",
        "B3\nr", "B4\nw","B4\nr","B5\nw", "B5\nr", "B6\nw",
        "B6\nr", "B7\nw","B7\nr"};


			for (i = 1; i < timeline::Width / initWidth; i++)
            {
				String ^dispStr = i.ToString();
				System::Drawing::Font ^strFont; 
				strFont = gcnew System::Drawing::Font("Consolas", 6);
				SolidBrush ^strBrush = gcnew SolidBrush(System::Drawing::Color::Black);
				formGraphics->DrawString(dispStr, strFont, strBrush, (i + 1) * initWidth - 10, 0);
				formGraphics->DrawLine(myPen, i * initWidth, 20, i * initWidth, timeline::Height);
				//strFont->Dispose();
                //strBrush.Dispose();
            }
				 for (i = 0; i < 17; i++)
            {
                formGraphics->DrawLine(myPen, 0, 20 + 40 * i,
					timeline::Width, 20 + 40 * i);
                if (i < 16)
                {
                    System::Drawing::Font ^ strFont;
					strFont = gcnew System::Drawing::Font("Consolas", 8);
                    SolidBrush ^strBrush;
					strBrush = gcnew SolidBrush(System::Drawing::Color::Black);
                    formGraphics->DrawString(buffers[i], strFont, strBrush, 0, 30 + 40 * i);
                  //  strFont.Dispose();
                  //  strBrush.Dispose();
                }
            }
				
///////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
				 /////////////////////////////////////////////

int j,k,l=0;
double node1_f[16],node2_f[16],node3_f[16],node4_f[16];
time_t *timer,*start;
double Cur_Titr,New_Titr[16],form_Titr;// Req_Titr, ;
double Power1, Power2, Power3, Power4, TotalPower_Dissipated[16];
fptr=fopen("C:\\BBDFlog1.txt","w+");
fptr2=fopen("C:\\BBDFlog2.txt","w+");

// Assume that the tool generator can give you only two frequencies



//--------------------------------------------------------------


//--------------------------------------------------------------
filltablewithdata();	//// table data check method

buff1 =			  node_create(1,  L1, L2,  f1,  f2 , M1, nw1 , nr1);//feedback path
buff2 =	list_insert_end(buff1,2,  L2, L3,  f2,  f3, M2, nw2 , nr2);//feedback path
buff8 =	list_insert_end(buff1,8,  L3, L1,  f3,  f1, M8, nw8 , nr8);//feedback path
buff3 =	list_insert_end(buff1,3,  L1, L4,  f1,  f4, M3, nw3 , nr3);//feedforward path
buff4 =	list_insert_end(buff1,4,  L4, L3,  f4,  f3, M4, nw4 , nr4);//feedforward path
buff5 =	list_insert_end(buff1,5,  L1, L1,  f1,  f1, M5, nw5 , nr5);
buff7 =	list_insert_end(buff1,7,  L4, L4,  f4,  f4, M7, nw7 , nr7);
buff6 =	list_insert_end(buff1,6,  L3, L3,  f3,  f3, M6, nw6 , nr6);

form_Titr = find_Titr(buff1,buff8);

buff5->start_read = buff8->start_read;
buff5->stop_read = buff8->stop_read;

buff5->start_write = buff5->start_read - buff5->nr*(1/buff5->f2);
buff5->stop_write = buff5->start_write + buff5->M*(1/buff5->f1);

buff4->start_read = buff2->start_read;
buff4->stop_read = buff2->stop_read;

	if(buff4->f1 >= buff4->f2) //  if f4 > f3
	{
	buff4->start_write = buff4->start_read - buff4->nr*(1/(buff4->f2));
	buff4->stop_write = buff4->start_write + (1/(buff4->f1))*buff4->M;
	}

	else
	{
	buff4->stop_write = buff4->start_read + 1/(buff4->f2)*buff4->M - (buff4->nr)*(1/(buff4->f2));
	buff4->start_write = buff4->stop_write - (1/(buff4->f1)) * buff4->M;
	}

	buff3->start_read = buff4->start_write - (buff4->latency1)*(1/(buff3->f2))-buff4->nw*(1/buff4->f1);
	buff3->stop_read = buff3->start_read + (1/(buff3->f2)) * buff3->M;

	if(buff3->f1 >= buff3->f2) //  if f1 > f4
	{
	buff3->start_write = buff3->start_read - buff3->nr*(1/(buff3->f2));
	buff3->stop_write = buff3->start_write + (1/(buff3->f1)) * buff3->M;
	}
	
	else
	{
	buff3->stop_write = buff3->start_read + 1/(buff3->f2)*buff3->M - (buff3->nr)*(1/(buff3->f2));
	buff3->start_write = buff3->stop_write - (1/(buff3->f1)) * buff3->M;
	}

	buff6->start_write = buff8->start_write;
	buff6->stop_write = buff8->stop_write;

	buff6->start_read = buff6->start_write + buff6->nr*(1/(buff6->f2)); // f1 == f2 so no D calculation required here
	buff6->stop_read = buff6->start_read + (1/(buff6->f2)) * buff6->M;

	buff7->start_read = buff3->start_read;
	buff7->stop_read = buff7->start_read + (1/(buff7->f2)) * buff7->M;
	
	buff7->start_write = buff7->start_read - buff7->nr *  (1/(buff7->f2));
	buff7->stop_write = buff7->start_write + (1/(buff7->f2)) * buff7->M;
	print_list(buff1);
	fprintf(fptr,"\n\n----------------------------WINAPI VALUES--------------------------------");

	computeDvalues(buff1);
 
	print_list(buff1);

	/////print the mem data here////////////////
	generatememorydata( buff1, 8.00);

	//Cur_Titr=print_timestamps(buff1,buff6);

/////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////We start drawing timeline here


////  B1 write
SolidBrush ^ brush1;
System::Drawing::Pen ^ myPen2;
brush1 = gcnew SolidBrush(System::Drawing::Color::Green);
//formGraphics->FillRectangle(brush1, System::Convert::ToInt32(initWidth+(buff1->start_write)*initWidth), 20 , System::Convert::ToInt32(initWidth *(buff1->stop_write - buff1->start_write)), 20);
//B1 write bars
myPen2 = gcnew System::Drawing::Pen(System::Drawing::Color::Black);
//for (i=0;i< System::Convert::ToInt32(buff1->M);i++)
//formGraphics->DrawLine(myPen2, System::Convert::ToInt32(initWidth+(buff1->start_write)*initWidth+initWidth/buff1->f1*i), 20 , System::Convert::ToInt32(initWidth+(buff1->start_write)*initWidth+initWidth/buff1->f1*i),  40);

////  B1 read
SolidBrush ^ brush2;
brush2 = gcnew SolidBrush(System::Drawing::Color::Gray);
//formGraphics->FillRectangle(brush2, System::Convert::ToInt32(initWidth+(buff1->start_read)*initWidth), 60 , System::Convert::ToInt32(initWidth *(buff1->stop_read - buff1->start_read) ), 20);
// B1 read bars
System::Drawing::Pen ^ myPen3;
myPen3 = gcnew System::Drawing::Pen(System::Drawing::Color::Black);
//for (i=0;i< System::Convert::ToInt32(buff1->M);i++)
//formGraphics->DrawLine(myPen3, System::Convert::ToInt32(initWidth+(buff1->start_read)*initWidth+initWidth/buff1->f2*i), 60 , System::Convert::ToInt32(initWidth+(buff1->start_read)*initWidth+initWidth/buff1->f2*i),  80);



////  B2 write
SolidBrush ^ brush3;
brush3 = gcnew SolidBrush(System::Drawing::Color::Blue);
//formGraphics->FillRectangle(brush3, System::Convert::ToInt32(initWidth+(buff2->start_write)*initWidth), 100 , System::Convert::ToInt32(initWidth *(buff2->stop_write - buff2->start_write) ), 20);
//B2 write bars
//for (i=0;i< System::Convert::ToInt32(buff2->M);i++)
//formGraphics->DrawLine(myPen3, System::Convert::ToInt32(initWidth+(buff2->start_write)*initWidth+initWidth/buff2->f1*i), 100 , System::Convert::ToInt32(initWidth+(buff2->start_write)*initWidth+initWidth/buff2->f1*i),  120);

////  B2 read
SolidBrush ^ brush4;
brush4 = gcnew SolidBrush(System::Drawing::Color::BurlyWood);
//formGraphics->FillRectangle(brush4, System::Convert::ToInt32(initWidth+(buff2->start_read)*initWidth), 140 , System::Convert::ToInt32(initWidth *(buff2->stop_read - buff2->start_read) ), 20);
//B2 read bars
//for (i=0;i< System::Convert::ToInt32(buff2->M);i++)
//formGraphics->DrawLine(myPen3, System::Convert::ToInt32(initWidth+(buff2->start_read)*initWidth+initWidth/buff2->f2*i), 140 , System::Convert::ToInt32(initWidth+(buff2->start_read)*initWidth+initWidth/buff2->f2*i),  160);


////  B8 write
SolidBrush ^ brush5;
brush5 = gcnew SolidBrush(System::Drawing::Color::Fuchsia);
//formGraphics->FillRectangle(brush5, System::Convert::ToInt32(initWidth+(buff8->start_write)*initWidth), 180 , System::Convert::ToInt32(initWidth *(buff8->stop_write - buff8->start_write)), 20);

//B8 write bars
//for (i=0;i< System::Convert::ToInt32(buff8->M);i++)
//formGraphics->DrawLine(myPen3, System::Convert::ToInt32(initWidth+(buff8->start_write)*initWidth+initWidth/buff8->f1*i), 180 , System::Convert::ToInt32(initWidth+(buff8->start_write)*initWidth+initWidth/buff8->f1*i),  200);

////  B8 read
SolidBrush ^ brush6;
brush6 = gcnew SolidBrush(System::Drawing::Color::Pink);
//formGraphics->FillRectangle(brush6, System::Convert::ToInt32(initWidth+(buff8->start_read)*initWidth), 220 , System::Convert::ToInt32(initWidth *(buff8->stop_read - buff8->start_read) ), 20);
//B8 read bars
//for (i=0;i< System::Convert::ToInt32(buff8->M);i++)
//formGraphics->DrawLine(myPen3, System::Convert::ToInt32(initWidth+(buff8->start_read)*initWidth+initWidth/buff8->f2*i), 220 , System::Convert::ToInt32(initWidth+(buff8->start_read)*initWidth+initWidth/buff8->f2*i),  240);


////  B3 write
SolidBrush ^ brush7;
brush7 = gcnew SolidBrush(System::Drawing::Color::Green);
//formGraphics->FillRectangle(brush7, System::Convert::ToInt32(initWidth+(buff3->start_write)*initWidth), 260 , System::Convert::ToInt32(initWidth *(buff3->stop_write - buff3->start_write)), 20);
//B3 write bars
//for (i=0;i< System::Convert::ToInt32(buff3->M);i++)
//formGraphics->DrawLine(myPen3, System::Convert::ToInt32(initWidth+(buff3->start_write)*initWidth+initWidth/buff3->f1*i), 260 , System::Convert::ToInt32(initWidth+(buff3->start_write)*initWidth+initWidth/buff3->f1*i),  280);

////  B3 read
SolidBrush ^ brush8;
brush8 = gcnew SolidBrush(System::Drawing::Color::Aquamarine);
//formGraphics->FillRectangle(brush8, System::Convert::ToInt32(initWidth+(buff3->start_read)*initWidth), 300 , System::Convert::ToInt32(initWidth *(buff3->stop_read - buff3->start_read) ), 20);

// B3 read bars
//for (i=0;i< System::Convert::ToInt32(buff3->M);i++)
//formGraphics->DrawLine(myPen3, System::Convert::ToInt32(initWidth+(buff3->start_read)*initWidth+initWidth/buff3->f2*i), 300 , System::Convert::ToInt32(initWidth+(buff3->start_read)*initWidth+initWidth/buff3->f2*i),  320);

////  B4 write
SolidBrush ^ brush9;
brush9 = gcnew SolidBrush(System::Drawing::Color::LightSteelBlue);
//formGraphics->FillRectangle(brush9, System::Convert::ToInt32(initWidth+(buff4->start_write)*initWidth), 340 , System::Convert::ToInt32(initWidth *(buff4->stop_write - buff4->start_write)), 20);
//B4 write bars
//for (i=0;i< System::Convert::ToInt32(buff4->M);i++)
//formGraphics->DrawLine(myPen3, System::Convert::ToInt32(initWidth+(buff4->start_write)*initWidth+initWidth/buff4->f1*i), 340 , System::Convert::ToInt32(initWidth+(buff4->start_write)*initWidth+initWidth/buff4->f1*i),  360);

////  B4 read
SolidBrush ^ brush10;
brush10 = gcnew SolidBrush(System::Drawing::Color::Crimson);
//formGraphics->FillRectangle(brush10, System::Convert::ToInt32(initWidth+(buff4->start_read)*initWidth), 380 , System::Convert::ToInt32(initWidth *(buff4->stop_read - buff4->start_read) ), 20);

// B4 read bars
//for (i=0;i< System::Convert::ToInt32(buff4->M);i++)
//formGraphics->DrawLine(myPen3, System::Convert::ToInt32(initWidth+(buff4->start_read)*initWidth+initWidth/buff4->f2*i), 380 , System::Convert::ToInt32(initWidth+(buff4->start_read)*initWidth+initWidth/buff4->f2*i),  400);

////  B5 write
SolidBrush ^ brush11;
brush11 = gcnew SolidBrush(System::Drawing::Color::Wheat);
formGraphics->FillRectangle(brush11, System::Convert::ToInt32(initWidth+(buff5->start_write)*initWidth), 420 , System::Convert::ToInt32(initWidth *(buff5->stop_write - buff5->start_write)), 20);
//B5 write bars
for (i=0;i< System::Convert::ToInt32(buff5->M);i++)
formGraphics->DrawLine(myPen3, System::Convert::ToInt32(initWidth+(buff5->start_write)*initWidth+initWidth/buff5->f1*i), 420 , System::Convert::ToInt32(initWidth+(buff5->start_write)*initWidth+initWidth/buff5->f1*i),  440);

////  B5 read
SolidBrush ^ brush12;
brush12 = gcnew SolidBrush(System::Drawing::Color::DarkSlateBlue);
formGraphics->FillRectangle(brush12, System::Convert::ToInt32(initWidth+(buff5->start_read)*initWidth), 460 , System::Convert::ToInt32( initWidth *(buff5->stop_read - buff5->start_read) ), 20);

// B5 read bars
for (i=0;i< System::Convert::ToInt32(buff5->M);i++)
formGraphics->DrawLine(myPen3, System::Convert::ToInt32(initWidth+(buff5->start_read)*initWidth+initWidth/buff5->f2*i), 460 , System::Convert::ToInt32(initWidth+(buff5->start_read)*initWidth+initWidth/buff5->f2*i),  480);

////  B6 write
SolidBrush ^ brush13;
brush13 = gcnew SolidBrush(System::Drawing::Color::Goldenrod);
//formGraphics->FillRectangle(brush13, System::Convert::ToInt32(initWidth+(buff6->start_write)*initWidth), 500 , System::Convert::ToInt32(initWidth *(buff6->stop_write - buff6->start_write)), 20);
//B6 write bars
//for (i=0;i< System::Convert::ToInt32(buff6->M);i++)
//formGraphics->DrawLine(myPen3, System::Convert::ToInt32(initWidth+(buff6->start_write)*initWidth+initWidth/buff6->f1*i), 500 , System::Convert::ToInt32(initWidth+(buff6->start_write)*initWidth+initWidth/buff6->f1*i),  520);

////  B6 read
SolidBrush ^ brush14;
brush14 = gcnew SolidBrush(System::Drawing::Color::LimeGreen);
//formGraphics->FillRectangle(brush14, System::Convert::ToInt32(initWidth+(buff6->start_read)*initWidth), 540 , System::Convert::ToInt32(initWidth *(buff6->stop_read - buff6->start_read) ), 20);

// B6 read bars
//for (i=0;i< System::Convert::ToInt32(buff6->M);i++)
//formGraphics->DrawLine(myPen3, System::Convert::ToInt32(initWidth+(buff6->start_read)*initWidth+initWidth/buff6->f2*i), 540 , System::Convert::ToInt32(initWidth+(buff6->start_read)*initWidth+initWidth/buff6->f2*i),  560);




////  B7 write
SolidBrush ^ brush15;
brush15 = gcnew SolidBrush(System::Drawing::Color::SteelBlue);
//formGraphics->FillRectangle(brush15, System::Convert::ToInt32(initWidth+(buff7->start_write)*initWidth), 580 , System::Convert::ToInt32(initWidth *(buff7->stop_write - buff7->start_write)), 20);
//B7 write bars
//for (i=0;i< System::Convert::ToInt32(buff7->M);i++)
//formGraphics->DrawLine(myPen3, System::Convert::ToInt32(initWidth+(buff7->start_write)*initWidth+initWidth/buff7->f1*i), 580 , System::Convert::ToInt32(initWidth+(buff7->start_write)*initWidth+initWidth/buff7->f1*i),  600);

////  B7 read
SolidBrush ^ brush16;
brush16 = gcnew SolidBrush(System::Drawing::Color::DarkKhaki);
//formGraphics->FillRectangle(brush16, System::Convert::ToInt32(initWidth+(buff7->start_read)*initWidth), 620 , System::Convert::ToInt32(initWidth *(buff7->stop_read - buff7->start_read) ), 20);

// B7 read bars
//for (i=0;i< System::Convert::ToInt32(buff7->M);i++)
//formGraphics->DrawLine(myPen3, System::Convert::ToInt32(initWidth+(buff7->start_read)*initWidth+initWidth/buff7->f2*i), 620 , System::Convert::ToInt32(initWidth+(buff7->start_read)*initWidth+initWidth/buff7->f2*i),  640);

//////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////DRAWING TITR///////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////

double offset =  initWidth*Req_Titr; // update the width to repeat the buffer bars.



formGraphics->FillRectangle(brush1, System::Convert::ToInt32(offset+(buff1->start_write)*initWidth), 40 , System::Convert::ToInt32(initWidth *(buff1->stop_write - buff1->start_write)), 20);
//B1 write bars

for (i=0;i< System::Convert::ToInt32(buff1->M);i++)
formGraphics->DrawLine(myPen2, System::Convert::ToInt32(offset+(buff1->start_write)*initWidth+initWidth/buff1->f1*i), 40 , System::Convert::ToInt32(offset+(buff1->start_write)*initWidth+initWidth/buff1->f1*i),  60);

////  B1 read


formGraphics->FillRectangle(brush2, System::Convert::ToInt32(offset+(buff1->start_read)*initWidth), 80 , System::Convert::ToInt32(initWidth *(buff1->stop_read - buff1->start_read) ), 20);
// B1 read bars


for (i=0;i< System::Convert::ToInt32(buff1->M);i++)
formGraphics->DrawLine(myPen3, System::Convert::ToInt32(offset+(buff1->start_read)*initWidth+initWidth/buff1->f2*i), 80, System::Convert::ToInt32(offset+(buff1->start_read)*initWidth+initWidth/buff1->f2*i),  100);

////  B2 write


formGraphics->FillRectangle(brush3, System::Convert::ToInt32(offset+(buff2->start_write)*initWidth), 120 , System::Convert::ToInt32(initWidth *(buff2->stop_write - buff2->start_write) ), 20);
//B2 write bars
for (i=0;i< System::Convert::ToInt32(buff2->M);i++)
formGraphics->DrawLine(myPen3, System::Convert::ToInt32(offset+(buff2->start_write)*initWidth+initWidth/buff2->f1*i), 120 , System::Convert::ToInt32(offset+(buff2->start_write)*initWidth+initWidth/buff2->f1*i),  140);

////  B2 read

formGraphics->FillRectangle(brush4, System::Convert::ToInt32(offset+(buff2->start_read)*initWidth), 160 , System::Convert::ToInt32(initWidth *(buff2->stop_read - buff2->start_read) ), 20);
//	B2 read bars
for (i=0;i< System::Convert::ToInt32(buff2->M);i++)
formGraphics->DrawLine(myPen3, System::Convert::ToInt32(offset+(buff2->start_read)*initWidth+initWidth/buff2->f2*i), 160 , System::Convert::ToInt32(offset+(buff2->start_read)*initWidth+initWidth/buff2->f2*i),  180);


////  B8 write

formGraphics->FillRectangle(brush5, System::Convert::ToInt32(offset+(buff8->start_write)*initWidth), 200 , System::Convert::ToInt32(initWidth *(buff8->stop_write - buff8->start_write)), 20);
//B8 write bars
for (i=0;i< System::Convert::ToInt32(buff8->M);i++)
formGraphics->DrawLine(myPen3, System::Convert::ToInt32(offset+(buff8->start_write)*initWidth+initWidth/buff8->f1*i), 200 , System::Convert::ToInt32(offset+(buff8->start_write)*initWidth+initWidth/buff8->f1*i),  220);

////  B8 read

formGraphics->FillRectangle(brush6, System::Convert::ToInt32(offset+(buff8->start_read)*initWidth), 240 , System::Convert::ToInt32(initWidth *(buff8->stop_read - buff8->start_read) ), 20);
//B8 read bars
for (i=0;i< System::Convert::ToInt32(buff8->M);i++)
formGraphics->DrawLine(myPen3, System::Convert::ToInt32(offset+(buff8->start_read)*initWidth+initWidth/buff8->f2*i), 240 , System::Convert::ToInt32(offset+(buff8->start_read)*initWidth+initWidth/buff8->f2*i),  260);


////  B3 write

formGraphics->FillRectangle(brush7, System::Convert::ToInt32(offset+(buff3->start_write)*initWidth), 280 , System::Convert::ToInt32(initWidth *(buff3->stop_write - buff3->start_write)), 20);
//B3 write bars
for (i=0;i< System::Convert::ToInt32(buff3->M);i++)
formGraphics->DrawLine(myPen3, System::Convert::ToInt32(offset+(buff3->start_write)*initWidth+initWidth/buff3->f1*i), 280 , System::Convert::ToInt32(offset+(buff3->start_write)*initWidth+initWidth/buff3->f1*i),  300);

////  B3 read

formGraphics->FillRectangle(brush8, System::Convert::ToInt32(offset+(buff3->start_read)*initWidth), 320 , System::Convert::ToInt32(initWidth *(buff3->stop_read - buff3->start_read) ), 20);

// B3 read bars
for (i=0;i< System::Convert::ToInt32(buff3->M);i++)
formGraphics->DrawLine(myPen3, System::Convert::ToInt32(offset+(buff3->start_read)*initWidth+initWidth/buff3->f2*i), 320 , System::Convert::ToInt32(offset+(buff3->start_read)*initWidth+initWidth/buff3->f2*i),  340);

////  B4 write
formGraphics->FillRectangle(brush9, System::Convert::ToInt32(offset+(buff4->start_write)*initWidth), 360 , System::Convert::ToInt32(initWidth *(buff4->stop_write - buff4->start_write)), 20);
//B4 write bars
for (i=0;i< System::Convert::ToInt32(buff4->M);i++)
formGraphics->DrawLine(myPen3, System::Convert::ToInt32(offset+(buff4->start_write)*initWidth+initWidth/buff4->f1*i), 360 , System::Convert::ToInt32(offset+(buff4->start_write)*initWidth+initWidth/buff4->f1*i),  380);

////  B4 read

formGraphics->FillRectangle(brush10, System::Convert::ToInt32(offset+(buff4->start_read)*initWidth), 400 , System::Convert::ToInt32(initWidth *(buff4->stop_read - buff4->start_read) ), 20);

// B4 read bars
for (i=0;i< System::Convert::ToInt32(buff4->M);i++)
formGraphics->DrawLine(myPen3, System::Convert::ToInt32(offset+(buff4->start_read)*initWidth+initWidth/buff4->f2*i), 400 , System::Convert::ToInt32(offset+(buff4->start_read)*initWidth+initWidth/buff4->f2*i),  420);



////  B5 write

formGraphics->FillRectangle(brush11, System::Convert::ToInt32(offset+(buff5->start_write)*initWidth), 440 , System::Convert::ToInt32(initWidth *(buff5->stop_write - buff5->start_write)), 20);
//B5 write bars
for (i=0;i< System::Convert::ToInt32(buff5->M);i++)
formGraphics->DrawLine(myPen3, System::Convert::ToInt32(offset+(buff5->start_write)*initWidth+initWidth/buff5->f1*i), 440 , System::Convert::ToInt32(offset+(buff5->start_write)*initWidth+initWidth/buff5->f1*i),  460);

////  B5 read

formGraphics->FillRectangle(brush12, System::Convert::ToInt32(offset+(buff5->start_read)*initWidth), 480 , System::Convert::ToInt32(initWidth *(buff5->stop_read - buff5->start_read) ), 20);

// B5 read bars
for (i=0;i< System::Convert::ToInt32(buff5->M);i++)
formGraphics->DrawLine(myPen3, System::Convert::ToInt32(offset+(buff5->start_read)*initWidth+initWidth/buff5->f2*i), 480 , System::Convert::ToInt32(offset+(buff5->start_read)*initWidth+initWidth/buff5->f2*i),  500);



////  B6 write

formGraphics->FillRectangle(brush13, System::Convert::ToInt32(offset+(buff6->start_write)*initWidth), 520 , System::Convert::ToInt32(initWidth *(buff6->stop_write - buff6->start_write)), 20);
//B6 write bars
for (i=0;i< System::Convert::ToInt32(buff6->M);i++)
formGraphics->DrawLine(myPen3, System::Convert::ToInt32(offset+(buff6->start_write)*initWidth+initWidth/buff6->f1*i), 520 , System::Convert::ToInt32(offset+(buff6->start_write)*initWidth+initWidth/buff6->f1*i),  540);

////  B6 read

formGraphics->FillRectangle(brush14, System::Convert::ToInt32(offset+(buff6->start_read)*initWidth), 560 , System::Convert::ToInt32(initWidth *(buff6->stop_read - buff6->start_read) ), 20);

// B6 read bars
for (i=0;i< System::Convert::ToInt32(buff6->M);i++)
formGraphics->DrawLine(myPen3, System::Convert::ToInt32(offset+(buff6->start_read)*initWidth+initWidth/buff6->f2*i),560 , System::Convert::ToInt32(offset+(buff6->start_read)*initWidth+initWidth/buff6->f2*i),  580);


////  B7 write

formGraphics->FillRectangle(brush15, System::Convert::ToInt32(offset+(buff7->start_write)*initWidth), 600 , System::Convert::ToInt32(initWidth *(buff7->stop_write - buff7->start_write)), 20);
//B7 write bars
for (i=0;i< System::Convert::ToInt32(buff7->M);i++)
formGraphics->DrawLine(myPen3, System::Convert::ToInt32(offset+(buff7->start_write)*initWidth+initWidth/buff7->f1*i), 600 , System::Convert::ToInt32(offset+(buff7->start_write)*initWidth+initWidth/buff7->f1*i),  620);

////  B7 read

formGraphics->FillRectangle(brush16, System::Convert::ToInt32(offset+(buff7->start_read)*initWidth), 640 , System::Convert::ToInt32(initWidth *(buff7->stop_read - buff7->start_read) ), 20);

// B7 read bars
for (i=0;i< System::Convert::ToInt32(buff7->M);i++)
formGraphics->DrawLine(myPen3, System::Convert::ToInt32(offset+(buff7->start_read)*initWidth+initWidth/buff7->f2*i), 640 , System::Convert::ToInt32(offset+(buff7->start_read)*initWidth+initWidth/buff7->f2*i),  660);

			 }	//end of load
	
};
}
