#pragma once

#include "stdio.h"
#include "stdlib.h"
#include <time.h>
FILE *fptr, *fptr2, *fptr3;
int main_bbdf(double ,double ,double ,double , double , double ,double );
time_t* get_time(void);
int M1,M2,M3,M4,M5,M6,M7,M8;
int nr1,nr2,nr3,nr4,nr5,nr6,nr7,nr8;
int nw1,nw2,nw3,nw4,nw5,nw6,nw7,nw8;
double (*curve_ptr)(double )=NULL;
double f1,f2,f3,f4;
double fclk_gen1, fclk_gen2, Req_Titr, Cur_Titr;
int L1 ;
int L2 ;
int L3 ;
int L4 ;
int L5 ;

#define MEMDATA_LENGTH 100
/////////////////////check how table stuff works/////////////////////////////////////
struct table_data
{
int intdata;
char str[20];
};
/////////////////////////////////////////////////////////////////////////////////////

typedef class Dbuffer
{
		public:
		double start_write; // start_write signal indicates that we can start writing INTO from the buffer
        double start_read; // start_read signal indicates that we can start reading OUT from the buffer
        double stop_write;
        double stop_read;
        double latency1;
        double latency2;
		double M;
        double f1;
        double f2;
		double nw;			// write offset
		double nr;			// read offset
        int data;
        double D; // in case if producer is slower than consumer
		double Power_at_freq( double, double (*curve_ptr)(double, double  ));
        class Dbuffer *next; // pointer to implement linked list

} * DBUFFER;

DBUFFER list;
DBUFFER l1,l2,l3,l4,buff1,buff2,buff8,buff3,buff4,buff5,buff7,buff6,l13,l14,l15,l16,l17,l18;
double find_Titr( DBUFFER , DBUFFER );

void filltablewithdata (void)
{
struct table_data table_obj[20];

for (int i =0; i < 20; i++)
table_obj[i].intdata = i;

}


typedef struct Titr_list {
 double  Titr_val;
 double Power_dissipation;
 double node1_freq;
 double node2_freq;
 double node3_freq;
 double node4_freq;
 struct Titr_list *next;	
} *TITR_LST;

TITR_LST Tlist;
TITR_LST topset;

DBUFFER node_create(int data, double l1, double l2, double freq1, double freq2, double M, int nw, int nr)
{
	DBUFFER buff;
	if(!(buff =(Dbuffer *) malloc(sizeof(class Dbuffer))))
	{
	fprintf(fptr,"\ncouldn't allocate memory");
	 return NULL;
	}
	buff->data=data;
	buff->next=NULL;
	buff->f1= freq1;
	buff->f2= freq2;
	buff->M = M;
	buff->latency1=l1;
	buff->latency2=l2;
	buff->nr=nr;
	buff->D=nr;
	buff->nw=nw;
	return buff;
}

DBUFFER list_insert_after(DBUFFER node, int data) 
{
		DBUFFER newnode;
        newnode=node_create(data,0,0,0,0,0,0,0);
        // while(node->next!=NULL) //insertat end always
        // node = node->next;  // change these two lines to add to a specific node.
        newnode->next = node->next;
        node->next = newnode;
        return newnode;
}

DBUFFER list_insert_end(DBUFFER node,int data,double l1,double l2, double freq1, double freq2, double M, int nw, int nr) 
{
		DBUFFER newnode;
        newnode= node_create(data,l1,l2,freq1,freq2,M,nw,nr);
        while(node->next!=NULL) // insert at end always
        node = node->next;  	// change these two lines to add to a specific node.
        node->next = newnode;
        newnode->next = NULL;
        return newnode;
}


int node_remove(DBUFFER list, DBUFFER node)
{
	while(list->next && list->next!=node) list=list->next;
	if(list->next) {
		list->next = node->next;
		free(node);
		return 0;
	} else return -1;
}

void print_list( DBUFFER list)
{

while(list!=NULL)
{
	fprintf(fptr2,"\nlist->data=%d", list->data);
	fprintf(fptr2,"\nlist->latency1=%lf", list->latency1);
	fprintf(fptr2,"\nlist->latency2=%lf", list->latency2);
	fprintf(fptr2,"\nlist->D=%lf", list->D);
	fprintf(fptr2,"\nlist->f1=%lf", list->f1);
	fprintf(fptr2,"\nlist->f2=%lf", list->f2);
	fprintf(fptr2,"\nlist->nw=%lf", list->nw);
	fprintf(fptr2,"\nlist->nr=%lf", list->nr);
	fprintf(fptr2,"\nlist->start_write=%lf", list->start_write);
	fprintf(fptr2,"\nlist->stop_write=%lf", list->stop_write);
	fprintf(fptr2,"\nlist->start_read=%lf", list->start_read);
	fprintf(fptr2,"\nlist->stop_read=%lf", list->stop_read);
	list=list->next;
}

}

//function to compute the iteration period of the loop given the start and end nodes

double find_Titr(DBUFFER start_node, DBUFFER end_node)
{	
	static int k=0;
	double Titr = 0;
	
	fprintf(fptr,"\nIteration %d",k);
	k++;
	start_node->start_write = start_node->latency1*(1/(start_node->f1)) + (start_node->nw)*(1/(start_node->f1));
	fprintf(fptr,"\nbuffer1 start_write = %lf",start_node->start_write);
	
	start_node->stop_write =  start_node->start_write+(1/(start_node->f1))*start_node->M;
	fprintf(fptr,"\nbuffer1 stop_write = %lf",start_node->stop_write);

	if	(start_node->f1 >= start_node->f2) //  adjustment for D and nr

		start_node->start_read = start_node->start_write + start_node->nr*(1/(start_node->f2));
	
	else
	
		start_node->start_read = start_node->stop_write - 1/(start_node->f2)*start_node->M + (start_node->nr)*(1/(start_node->f2)); // we can shrink another cycle
	
	
	fprintf(fptr,"\nbuffer1 start_read = %lf",start_node->start_read);

	start_node->stop_read = start_node->start_read + 1/(start_node->f2)* start_node->M ;
	fprintf(fptr,"\nbuffer1 stop_read = %lf",start_node->stop_read);


	int i=1;
	do
	{
		i++;
		
		start_node->next->start_write = start_node->next->latency1*(1/(start_node->next->f1)) +start_node->next->nw*(1/(start_node->f1))+ start_node->start_read;
		fprintf(fptr,"\n\nbuffer%d start_write = %lf",i,start_node->next->start_write);

		start_node->next->stop_write =  start_node->next->start_write+(1/(start_node->next->f1))*start_node->next->M;
		
		fprintf(fptr,"\nbuffer%d stop_write = %lf",i,start_node->next->stop_write);
		
		if(start_node->next->f1 >= start_node->next->f2) // adjustment for D and nr
		start_node->next->start_read = start_node->next->start_write+start_node->next->nr*(1/(start_node->f2));
		
		else
		start_node->next->start_read = start_node->next->stop_write - 1/(start_node->next->f2)*start_node->next->M+start_node->next->nr*(1/(start_node->f2));
		
		fprintf(fptr,"\nbuffer%d start_read = %lf",i,start_node->next->start_read);

		start_node->next->stop_read = start_node->next->start_read + 1/(start_node->next->f2)* start_node->next->M;
		fprintf(fptr,"\nbuffer%d stop_read = %lf",i,start_node->next->stop_read);

		start_node = start_node->next;
	}	while(start_node != end_node && start_node->next!= NULL);
	Titr = end_node->start_read;
	return Titr ;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
double computeDvalues(DBUFFER start_node)
{
while (start_node!=NULL)
{
	start_node->D = start_node->start_read - start_node->start_write;

start_node = start_node->next;
}
return 0;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
double print_timestamps(DBUFFER start_node, DBUFFER end_node)
{	
	
	double Titr = 0;
	int k=0;
	fprintf(fptr,"\n----------TIME STAMPS----------");
	

	while(start_node != NULL)
	{
		
		k++;
		fprintf(fptr,"\n\nbuffer%d start_write = %lf",start_node->data,start_node->start_write);
		fprintf(fptr,"\nbuffer%d stop_write = %lf",start_node->data,start_node->stop_write);
		fprintf(fptr,"\nbuffer%d start_read = %lf",start_node->data,start_node->start_read);

		fprintf(fptr,"\nbuffer%d stop_read = %lf",start_node->data,start_node->stop_read);

		start_node = start_node->next;
	}	
	
return 0;

}
////////////////////////////////////////////////////////////////////////////////
double maxfreq_calc( double arr[])
{
	double max =0;
	int i;
	for ( i=0; i<4;i++)
	{
	if (arr[i] > max && arr[i]!=0)
		max = arr[i];
	}
	return max;
}
////////////////////////////////////////////////////////////////////////////////

void generatememorydata(DBUFFER start_node, double maxfreq)
{
double loop=1;
int i,j;
int memsize = MEMDATA_LENGTH * (int) maxfreq;
int *ptr[16];

for(i=0;i<16;i++)
ptr[i] = (int *) malloc(sizeof(int)*memsize);

fptr3 = fopen("C:\\memdata.txt","w+");
DBUFFER temp = (DBUFFER) malloc(sizeof(Dbuffer));
temp = 	start_node;

	for ( j =0; j<memsize; j++)
	{
		fprintf(fptr3,"\n:loop=%lf\t",loop);
	temp = start_node;	
	for (i=0; i<15; i=i+2) // loop for one iteration of memory instance
	{
	//// if start_read is to be triggered at current time instance ////
	if (temp->start_write == loop)
	ptr[i][j]=1;
	else
	ptr[i][j]=0;

	//// if start_write is to be triggered at current time instance ////
	if (temp->start_read == loop)
	ptr[i+1][j]=1;
	else
	ptr[i+1][j]=0;
	/////////////////write data to file/////////////////////////////////////////////
	fprintf(fptr3,"%d%d",ptr[i][j],ptr[i+1][j]);

	temp = temp->next;
	}
	
	loop+=  1/maxfreq;
	}
	fclose(fptr3);

}
	////////////////////////////////////////////////////////////////////////////////
	//update the frequencies at the data buffer to perform optimization
void update_Dbuffer(DBUFFER node, double freq1, double freq2)
{	
	node->f1=freq1;	
	node->f2=freq2;
}

double  Power_at_freq( double freq, double (*curve_ptr)(double  ))
{
 double Power =  curve_ptr(freq);

 return Power;
}

//example passing  Power_at_freq( f1, &NonLinear1 );





double Total_PowerDissipation( double Power1, double Power2, double Power3, double Power4 )

{
	return Power1+Power2+Power3+Power4;

}



//select a suitable Power vs Frequency curve using a function pointer

double Linear1  (double freq) {
	double Power_Dissipation;
	Power_Dissipation = freq;
	return Power_Dissipation;
}

double Linear2  (double freq) {
	double Power_Dissipation;
	Power_Dissipation = freq*2;
	return Power_Dissipation;
}

double NonLinear1  (double freq) {
	double Power_Dissipation;
	if (freq>0 && freq <= 150)
	Power_Dissipation = freq*freq/2;
	else 
	if ( freq >= 150)
	{
	Power_Dissipation = freq/3+85;
	}
	return Power_Dissipation;
}

double NonLinear2  (double freq) {
	double Power_Dissipation;
	if (freq>0 && freq <= 100)
	Power_Dissipation = freq*freq/4;
	else if (freq > 100 && freq <= 200)
	{
	Power_Dissipation = freq/2+110;
	}
	else if (freq > 200)
	{
	Power_Dissipation = freq+225;
	}
	return Power_Dissipation;
}


/////////////////////////////////////////////////////////////////


TITR_LST create_node(double Titer, double Powr_dissipation, double freq1, double freq2, double freq3, double freq4)
{
	TITR_LST buff;
	if(!(buff =(Titr_list *) malloc(sizeof(struct Titr_list))))
	{
	fprintf(fptr,"\ncouldn't allocate memory");
	 return NULL;
	}
	buff->node1_freq=freq1;
	buff->node2_freq=freq2;
	buff->node3_freq= freq3;
	buff->node4_freq= freq4;
	buff->Titr_val=Titer;
	buff->Power_dissipation=Powr_dissipation;
	buff->next=NULL;
	return buff;
}

void insert_at_end(TITR_LST lst , double New_Titr , double TotalPower_Dissipated,double node1_f,double node2_f,double node3_f,double node4_f)
{
		TITR_LST newnode;
        newnode = create_node(New_Titr, TotalPower_Dissipated, node1_f, node2_f,node3_f, node4_f);
        while(lst->next!=NULL) 
        lst = lst->next;  	
        lst->next = newnode;
        newnode->next = NULL;
		
}




void prnt_lst( TITR_LST lst)
{

while(lst!=NULL)
{
	fprintf(fptr,"\n\nTitr_list->Titr_val=%lf", lst->Titr_val);
	fprintf(fptr,"\nTitr_list->Power_dissipation=%lf", lst->Power_dissipation);
	fprintf(fptr,"\nTitr_list->node1_freq=%lf", lst->node1_freq);
	fprintf(fptr,"\nTitr_list->node2_freq=%lf", lst->node2_freq);
	fprintf(fptr,"\nTitr_list->node3_freq=%lf", lst->node3_freq);
	fprintf(fptr,"\nTitr_list->node4_freq=%lf", lst->node4_freq);
	lst=lst->next;
}

}

TITR_LST find_best_set(TITR_LST list )
{
	double min;
	int v=0;
	TITR_LST tempnode; 
	tempnode = create_node(0,0,0,0,0,0);//create a temporary node to store best match
	
	do 
	{
	v=v+1;
	if (v==1)
	min = list->Power_dissipation;
	if ( list->Power_dissipation <= min )
	{
	min = list->Power_dissipation;
	tempnode->Power_dissipation = min;
	tempnode->node1_freq = list->node1_freq;
	tempnode->node2_freq = list->node2_freq;
	tempnode->node3_freq = list->node3_freq;
	tempnode->node4_freq = list->node4_freq;
	tempnode->Titr_val = list->Titr_val;
	tempnode->next=NULL;
	}
	list=list->next;
	}while(list!=NULL);
	return tempnode;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
