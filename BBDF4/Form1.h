#pragma once
#include "stdio.h"
#include <stdarg.h>
#include <ctype.h>
#include <Windows.h>
#include "timeline.h"
#include <ShellApi.h>
//#pragma comment(lib,"shell32.lib");
namespace BBDF4 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Form1
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();

			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}

	protected: 
	public: static timeline^ frm = gcnew timeline;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::TextBox^  textBox2;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::TextBox^  textBox3;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::TextBox^  textBox4;
	private: System::Windows::Forms::Label^  labebuff1;
	private: System::Windows::Forms::TextBox^  textBox5;
	private: System::Windows::Forms::Label^  labebuff2;
	private: System::Windows::Forms::TextBox^  textBox6;
	private: System::Windows::Forms::Label^  labebuff8;
	private: System::Windows::Forms::TextBox^  textBox7;
	private: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::Button^  button4;
	private: System::Windows::Forms::Label^  labebuff3;
	private: System::Windows::Forms::TextBox^  textBox8;
	private: System::Windows::Forms::Label^  labebuff4;
	private: System::Windows::Forms::TextBox^  textBox9;
	private: System::Windows::Forms::Label^  labebuff5;
	private: System::Windows::Forms::TextBox^  textBox10;
	private: System::Windows::Forms::Label^  labebuff7;
	private: System::Windows::Forms::TextBox^  textBox11;
	private: System::Windows::Forms::Button^  button5;
	private: System::Windows::Forms::PictureBox^  pictureBox1;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::PictureBox^  pictureBox2;
	private: System::Windows::Forms::TextBox^  textBox12;
	private: System::Windows::Forms::TextBox^  textBox13;
	private: System::Windows::Forms::TextBox^  textBox14;
	private: System::Windows::Forms::TextBox^  textBox15;
	private: System::Windows::Forms::TextBox^  textBox16;
	private: System::Windows::Forms::TextBox^  textBox17;
	private: System::Windows::Forms::TextBox^  textBox18;
	private: System::Windows::Forms::TextBox^  textBox19;
	private: System::Windows::Forms::TextBox^  textBox20;
	private: System::Windows::Forms::TextBox^  textBox21;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::Label^  label8;
	private: System::Windows::Forms::Label^  label9;
	private: System::Windows::Forms::Label^  label10;
	private: System::Windows::Forms::Label^  label11;
	private: System::Windows::Forms::Label^  label12;
	private: System::Windows::Forms::Label^  label13;
	private: System::Windows::Forms::PictureBox^  pictureBox3;
	private: System::Windows::Forms::TextBox^  textBox22;
	private: System::Windows::Forms::TextBox^  textBox23;
	private: System::Windows::Forms::TextBox^  textBox24;
	private: System::Windows::Forms::TextBox^  textBox25;
	private: System::Windows::Forms::TextBox^  textBox26;
	private: System::Windows::Forms::TextBox^  textBox27;
	private: System::Windows::Forms::TextBox^  textBox28;
	private: System::Windows::Forms::TextBox^  textBox29;
	private: System::Windows::Forms::TextBox^  textBox30;
	private: System::Windows::Forms::TextBox^  textBox31;
	private: System::Windows::Forms::TextBox^  textBox32;
	private: System::Windows::Forms::TextBox^  textBox33;
	private: System::Windows::Forms::TextBox^  textBox34;
	private: System::Windows::Forms::TextBox^  textBox35;
	private: System::Windows::Forms::PictureBox^  pictureBox4;
	private: System::Windows::Forms::Label^  label14;
	private: System::Windows::Forms::TextBox^  textBox36;
	private: System::Windows::Forms::TextBox^  textBox37;
	private: System::Windows::Forms::TextBox^  textBox38;
	private: System::Windows::Forms::TextBox^  textBox39;
	private: System::Windows::Forms::TextBox^  textBox40;
	private: System::Windows::Forms::TextBox^  textBox41;
	private: System::Windows::Forms::TextBox^  textBox42;
	private: System::Windows::Forms::TextBox^  textBox43;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::TextBox^  textBox44;
	private: System::Windows::Forms::Label^  label15;
	private: System::Windows::Forms::Button^  button6;



private: System::Windows::Forms::BindingSource^  tabledataBindingSource;


private: System::ComponentModel::IContainer^  components;






	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->textBox4 = (gcnew System::Windows::Forms::TextBox());
			this->labebuff1 = (gcnew System::Windows::Forms::Label());
			this->textBox5 = (gcnew System::Windows::Forms::TextBox());
			this->labebuff2 = (gcnew System::Windows::Forms::Label());
			this->textBox6 = (gcnew System::Windows::Forms::TextBox());
			this->labebuff8 = (gcnew System::Windows::Forms::Label());
			this->textBox7 = (gcnew System::Windows::Forms::TextBox());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->labebuff3 = (gcnew System::Windows::Forms::Label());
			this->textBox8 = (gcnew System::Windows::Forms::TextBox());
			this->labebuff4 = (gcnew System::Windows::Forms::Label());
			this->textBox9 = (gcnew System::Windows::Forms::TextBox());
			this->labebuff5 = (gcnew System::Windows::Forms::Label());
			this->textBox10 = (gcnew System::Windows::Forms::TextBox());
			this->labebuff7 = (gcnew System::Windows::Forms::Label());
			this->textBox11 = (gcnew System::Windows::Forms::TextBox());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->pictureBox2 = (gcnew System::Windows::Forms::PictureBox());
			this->textBox12 = (gcnew System::Windows::Forms::TextBox());
			this->textBox13 = (gcnew System::Windows::Forms::TextBox());
			this->textBox14 = (gcnew System::Windows::Forms::TextBox());
			this->textBox15 = (gcnew System::Windows::Forms::TextBox());
			this->textBox16 = (gcnew System::Windows::Forms::TextBox());
			this->textBox17 = (gcnew System::Windows::Forms::TextBox());
			this->textBox18 = (gcnew System::Windows::Forms::TextBox());
			this->textBox19 = (gcnew System::Windows::Forms::TextBox());
			this->textBox20 = (gcnew System::Windows::Forms::TextBox());
			this->textBox21 = (gcnew System::Windows::Forms::TextBox());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->label12 = (gcnew System::Windows::Forms::Label());
			this->label13 = (gcnew System::Windows::Forms::Label());
			this->pictureBox3 = (gcnew System::Windows::Forms::PictureBox());
			this->textBox22 = (gcnew System::Windows::Forms::TextBox());
			this->textBox23 = (gcnew System::Windows::Forms::TextBox());
			this->textBox24 = (gcnew System::Windows::Forms::TextBox());
			this->textBox25 = (gcnew System::Windows::Forms::TextBox());
			this->textBox26 = (gcnew System::Windows::Forms::TextBox());
			this->textBox27 = (gcnew System::Windows::Forms::TextBox());
			this->textBox28 = (gcnew System::Windows::Forms::TextBox());
			this->textBox29 = (gcnew System::Windows::Forms::TextBox());
			this->textBox30 = (gcnew System::Windows::Forms::TextBox());
			this->textBox31 = (gcnew System::Windows::Forms::TextBox());
			this->textBox32 = (gcnew System::Windows::Forms::TextBox());
			this->textBox33 = (gcnew System::Windows::Forms::TextBox());
			this->textBox34 = (gcnew System::Windows::Forms::TextBox());
			this->textBox35 = (gcnew System::Windows::Forms::TextBox());
			this->pictureBox4 = (gcnew System::Windows::Forms::PictureBox());
			this->label14 = (gcnew System::Windows::Forms::Label());
			this->textBox36 = (gcnew System::Windows::Forms::TextBox());
			this->textBox37 = (gcnew System::Windows::Forms::TextBox());
			this->textBox38 = (gcnew System::Windows::Forms::TextBox());
			this->textBox39 = (gcnew System::Windows::Forms::TextBox());
			this->textBox40 = (gcnew System::Windows::Forms::TextBox());
			this->textBox41 = (gcnew System::Windows::Forms::TextBox());
			this->textBox42 = (gcnew System::Windows::Forms::TextBox());
			this->textBox43 = (gcnew System::Windows::Forms::TextBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->textBox44 = (gcnew System::Windows::Forms::TextBox());
			this->label15 = (gcnew System::Windows::Forms::Label());
			this->button6 = (gcnew System::Windows::Forms::Button());
			this->tabledataBindingSource = (gcnew System::Windows::Forms::BindingSource(this->components));
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox4))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->tabledataBindingSource))->BeginInit();
			this->SuspendLayout();
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(383, 12);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(176, 56);
			this->button2->TabIndex = 1;
			this->button2->Text = L"GENERATE TIMELINE";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &Form1::button2_Click);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->BackColor = System::Drawing::Color::Gray;
			this->label1->Location = System::Drawing::Point(48, 249);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(57, 13);
			this->label1->TabIndex = 2;
			this->label1->Text = L"Frequency";
			this->label1->Click += gcnew System::EventHandler(this, &Form1::label1_Click);
			// 
			// textBox1
			// 
			this->textBox1->ForeColor = System::Drawing::Color::Black;
			this->textBox1->Location = System::Drawing::Point(38, 265);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(72, 20);
			this->textBox1->TabIndex = 3;
			this->textBox1->TabStop = false;
			this->textBox1->TextChanged += gcnew System::EventHandler(this, &Form1::textBox1_TextChanged);
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(38, 313);
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(72, 20);
			this->textBox2->TabIndex = 4;
			this->textBox2->TextChanged += gcnew System::EventHandler(this, &Form1::textBox2_TextChanged);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->BackColor = System::Drawing::Color::Cyan;
			this->label2->Location = System::Drawing::Point(5, 313);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(27, 13);
			this->label2->TabIndex = 5;
			this->label2->Text = L"PE2";
			// 
			// textBox3
			// 
			this->textBox3->Location = System::Drawing::Point(38, 364);
			this->textBox3->Name = L"textBox3";
			this->textBox3->Size = System::Drawing::Size(71, 20);
			this->textBox3->TabIndex = 6;
			this->textBox3->TextChanged += gcnew System::EventHandler(this, &Form1::textBox3_TextChanged);
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->BackColor = System::Drawing::Color::Cyan;
			this->label3->Location = System::Drawing::Point(5, 364);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(27, 13);
			this->label3->TabIndex = 7;
			this->label3->Text = L"PE3";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->BackColor = System::Drawing::Color::Cyan;
			this->label4->Location = System::Drawing::Point(5, 415);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(27, 13);
			this->label4->TabIndex = 8;
			this->label4->Text = L"PE4";
			// 
			// textBox4
			// 
			this->textBox4->Location = System::Drawing::Point(38, 415);
			this->textBox4->Name = L"textBox4";
			this->textBox4->Size = System::Drawing::Size(72, 20);
			this->textBox4->TabIndex = 9;
			this->textBox4->TextChanged += gcnew System::EventHandler(this, &Form1::textBox4_TextChanged);
			// 
			// labebuff1
			// 
			this->labebuff1->AutoSize = true;
			this->labebuff1->Location = System::Drawing::Point(711, 12);
			this->labebuff1->Name = L"labebuff1";
			this->labebuff1->Size = System::Drawing::Size(111, 13);
			this->labebuff1->TabIndex = 10;
			this->labebuff1->Text = L"clock generator freq 1";
			// 
			// textBox5
			// 
			this->textBox5->Location = System::Drawing::Point(723, 32);
			this->textBox5->Name = L"textBox5";
			this->textBox5->Size = System::Drawing::Size(88, 20);
			this->textBox5->TabIndex = 11;
			this->textBox5->TextChanged += gcnew System::EventHandler(this, &Form1::textBox5_TextChanged);
			// 
			// labebuff2
			// 
			this->labebuff2->AutoSize = true;
			this->labebuff2->Location = System::Drawing::Point(711, 56);
			this->labebuff2->Name = L"labebuff2";
			this->labebuff2->Size = System::Drawing::Size(111, 13);
			this->labebuff2->TabIndex = 12;
			this->labebuff2->Text = L"clock generator freq 2";
			// 
			// textBox6
			// 
			this->textBox6->Location = System::Drawing::Point(722, 71);
			this->textBox6->Name = L"textBox6";
			this->textBox6->Size = System::Drawing::Size(89, 20);
			this->textBox6->TabIndex = 13;
			this->textBox6->TextChanged += gcnew System::EventHandler(this, &Form1::textBox6_TextChanged);
			// 
			// labebuff8
			// 
			this->labebuff8->AutoSize = true;
			this->labebuff8->Location = System::Drawing::Point(611, 15);
			this->labebuff8->Name = L"labebuff8";
			this->labebuff8->Size = System::Drawing::Size(68, 13);
			this->labebuff8->TabIndex = 14;
			this->labebuff8->Text = L"Required Titr";
			// 
			// textBox7
			// 
			this->textBox7->Location = System::Drawing::Point(601, 31);
			this->textBox7->Name = L"textBox7";
			this->textBox7->Size = System::Drawing::Size(89, 20);
			this->textBox7->TabIndex = 15;
			this->textBox7->TextChanged += gcnew System::EventHandler(this, &Form1::textBox7_TextChanged);
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(383, 123);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(78, 43);
			this->button3->TabIndex = 16;
			this->button3->Text = L"EXIT TIMELINE";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &Form1::button3_Click);
			// 
			// button4
			// 
			this->button4->Location = System::Drawing::Point(483, 123);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(74, 43);
			this->button4->TabIndex = 17;
			this->button4->Text = L"EXIT";
			this->button4->UseVisualStyleBackColor = true;
			this->button4->Click += gcnew System::EventHandler(this, &Form1::button4_Click);
			// 
			// labebuff3
			// 
			this->labebuff3->AutoSize = true;
			this->labebuff3->BackColor = System::Drawing::Color::Peru;
			this->labebuff3->Location = System::Drawing::Point(297, 245);
			this->labebuff3->Name = L"labebuff3";
			this->labebuff3->Size = System::Drawing::Size(76, 13);
			this->labebuff3->TabIndex = 18;
			this->labebuff3->Text = L"Buffer Size (M)";
			// 
			// textBox8
			// 
			this->textBox8->Location = System::Drawing::Point(287, 261);
			this->textBox8->Name = L"textBox8";
			this->textBox8->Size = System::Drawing::Size(97, 20);
			this->textBox8->TabIndex = 19;
			this->textBox8->TextChanged += gcnew System::EventHandler(this, &Form1::textBox8_TextChanged);
			// 
			// labebuff4
			// 
			this->labebuff4->AutoSize = true;
			this->labebuff4->BackColor = System::Drawing::Color::Gray;
			this->labebuff4->Location = System::Drawing::Point(137, 249);
			this->labebuff4->Name = L"labebuff4";
			this->labebuff4->Size = System::Drawing::Size(60, 13);
			this->labebuff4->TabIndex = 20;
			this->labebuff4->Text = L"Latency (L)";
			// 
			// textBox9
			// 
			this->textBox9->Location = System::Drawing::Point(127, 268);
			this->textBox9->Name = L"textBox9";
			this->textBox9->Size = System::Drawing::Size(94, 20);
			this->textBox9->TabIndex = 21;
			this->textBox9->TextChanged += gcnew System::EventHandler(this, &Form1::textBox9_TextChanged);
			// 
			// labebuff5
			// 
			this->labebuff5->AutoSize = true;
			this->labebuff5->BackColor = System::Drawing::Color::Peru;
			this->labebuff5->Location = System::Drawing::Point(400, 246);
			this->labebuff5->Name = L"labebuff5";
			this->labebuff5->Size = System::Drawing::Size(84, 13);
			this->labebuff5->TabIndex = 22;
			this->labebuff5->Text = L"write offset ( nw)";
			// 
			// textBox10
			// 
			this->textBox10->Location = System::Drawing::Point(397, 261);
			this->textBox10->Name = L"textBox10";
			this->textBox10->Size = System::Drawing::Size(87, 20);
			this->textBox10->TabIndex = 23;
			this->textBox10->TextChanged += gcnew System::EventHandler(this, &Form1::textBox10_TextChanged_1);
			// 
			// labebuff7
			// 
			this->labebuff7->AutoSize = true;
			this->labebuff7->BackColor = System::Drawing::Color::Peru;
			this->labebuff7->Location = System::Drawing::Point(505, 246);
			this->labebuff7->Name = L"labebuff7";
			this->labebuff7->Size = System::Drawing::Size(78, 13);
			this->labebuff7->TabIndex = 24;
			this->labebuff7->Text = L"read offset ( nr)";
			// 
			// textBox11
			// 
			this->textBox11->Location = System::Drawing::Point(499, 261);
			this->textBox11->Name = L"textBox11";
			this->textBox11->Size = System::Drawing::Size(86, 20);
			this->textBox11->TabIndex = 25;
			this->textBox11->TextChanged += gcnew System::EventHandler(this, &Form1::textBox11_TextChanged);
			// 
			// button5
			// 
			this->button5->Location = System::Drawing::Point(508, 71);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(49, 46);
			this->button5->TabIndex = 26;
			this->button5->Text = L"refresh";
			this->button5->UseVisualStyleBackColor = true;
			this->button5->Click += gcnew System::EventHandler(this, &Form1::button5_Click);
			// 
			// pictureBox1
			// 
			this->pictureBox1->ImageLocation = L"C:\\Users\\SAMBHAV\\Documents\\Visual Studio 2008\\Projects\\BBDF4\\BBDF4\\Dataflow1.jpg";
			this->pictureBox1->Location = System::Drawing::Point(12, 12);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(351, 208);
			this->pictureBox1->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox1->TabIndex = 27;
			this->pictureBox1->TabStop = false;
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->BackColor = System::Drawing::Color::Cyan;
			this->label5->Location = System::Drawing::Point(5, 268);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(27, 13);
			this->label5->TabIndex = 28;
			this->label5->Text = L"PE1";
			// 
			// pictureBox2
			// 
			this->pictureBox2->BackColor = System::Drawing::Color::Gray;
			this->pictureBox2->Location = System::Drawing::Point(-1, 242);
			this->pictureBox2->Name = L"pictureBox2";
			this->pictureBox2->Size = System::Drawing::Size(242, 213);
			this->pictureBox2->TabIndex = 29;
			this->pictureBox2->TabStop = false;
			// 
			// textBox12
			// 
			this->textBox12->Location = System::Drawing::Point(287, 283);
			this->textBox12->Name = L"textBox12";
			this->textBox12->Size = System::Drawing::Size(97, 20);
			this->textBox12->TabIndex = 30;
			this->textBox12->TextChanged += gcnew System::EventHandler(this, &Form1::textBox12_TextChanged);
			// 
			// textBox13
			// 
			this->textBox13->Location = System::Drawing::Point(287, 327);
			this->textBox13->Name = L"textBox13";
			this->textBox13->Size = System::Drawing::Size(97, 20);
			this->textBox13->TabIndex = 31;
			this->textBox13->TextChanged += gcnew System::EventHandler(this, &Form1::textBox13_TextChanged);
			// 
			// textBox14
			// 
			this->textBox14->Location = System::Drawing::Point(287, 305);
			this->textBox14->Name = L"textBox14";
			this->textBox14->Size = System::Drawing::Size(97, 20);
			this->textBox14->TabIndex = 32;
			this->textBox14->TextChanged += gcnew System::EventHandler(this, &Form1::textBox14_TextChanged);
			// 
			// textBox15
			// 
			this->textBox15->Location = System::Drawing::Point(127, 364);
			this->textBox15->Name = L"textBox15";
			this->textBox15->Size = System::Drawing::Size(94, 20);
			this->textBox15->TabIndex = 33;
			this->textBox15->TextChanged += gcnew System::EventHandler(this, &Form1::textBox15_TextChanged);
			// 
			// textBox16
			// 
			this->textBox16->Location = System::Drawing::Point(127, 313);
			this->textBox16->Name = L"textBox16";
			this->textBox16->Size = System::Drawing::Size(94, 20);
			this->textBox16->TabIndex = 34;
			this->textBox16->TextChanged += gcnew System::EventHandler(this, &Form1::textBox16_TextChanged);
			// 
			// textBox17
			// 
			this->textBox17->Location = System::Drawing::Point(127, 415);
			this->textBox17->Name = L"textBox17";
			this->textBox17->Size = System::Drawing::Size(94, 20);
			this->textBox17->TabIndex = 35;
			this->textBox17->TextChanged += gcnew System::EventHandler(this, &Form1::textBox17_TextChanged);
			// 
			// textBox18
			// 
			this->textBox18->Location = System::Drawing::Point(287, 349);
			this->textBox18->Name = L"textBox18";
			this->textBox18->Size = System::Drawing::Size(97, 20);
			this->textBox18->TabIndex = 36;
			this->textBox18->TextChanged += gcnew System::EventHandler(this, &Form1::textBox18_TextChanged);
			// 
			// textBox19
			// 
			this->textBox19->Location = System::Drawing::Point(287, 371);
			this->textBox19->Name = L"textBox19";
			this->textBox19->Size = System::Drawing::Size(97, 20);
			this->textBox19->TabIndex = 37;
			this->textBox19->TextChanged += gcnew System::EventHandler(this, &Form1::textBox19_TextChanged);
			// 
			// textBox20
			// 
			this->textBox20->Location = System::Drawing::Point(287, 392);
			this->textBox20->Name = L"textBox20";
			this->textBox20->Size = System::Drawing::Size(97, 20);
			this->textBox20->TabIndex = 38;
			this->textBox20->TextChanged += gcnew System::EventHandler(this, &Form1::textBox20_TextChanged);
			// 
			// textBox21
			// 
			this->textBox21->Location = System::Drawing::Point(287, 413);
			this->textBox21->Name = L"textBox21";
			this->textBox21->Size = System::Drawing::Size(97, 20);
			this->textBox21->TabIndex = 39;
			this->textBox21->TextChanged += gcnew System::EventHandler(this, &Form1::textBox21_TextChanged);
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(259, 262);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(22, 13);
			this->label6->TabIndex = 40;
			this->label6->Text = L"M1";
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(260, 415);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(22, 13);
			this->label7->TabIndex = 41;
			this->label7->Text = L"M8";
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(259, 286);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(22, 13);
			this->label8->TabIndex = 42;
			this->label8->Text = L"M2";
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Location = System::Drawing::Point(259, 394);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(22, 13);
			this->label9->TabIndex = 43;
			this->label9->Text = L"M7";
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Location = System::Drawing::Point(259, 376);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(22, 13);
			this->label10->TabIndex = 44;
			this->label10->Text = L"M6";
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->Location = System::Drawing::Point(259, 353);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(22, 13);
			this->label11->TabIndex = 45;
			this->label11->Text = L"M5";
			// 
			// label12
			// 
			this->label12->AutoSize = true;
			this->label12->Location = System::Drawing::Point(259, 330);
			this->label12->Name = L"label12";
			this->label12->Size = System::Drawing::Size(22, 13);
			this->label12->TabIndex = 46;
			this->label12->Text = L"M4";
			// 
			// label13
			// 
			this->label13->AutoSize = true;
			this->label13->Location = System::Drawing::Point(259, 309);
			this->label13->Name = L"label13";
			this->label13->Size = System::Drawing::Size(22, 13);
			this->label13->TabIndex = 47;
			this->label13->Text = L"M3";
			// 
			// pictureBox3
			// 
			this->pictureBox3->BackColor = System::Drawing::Color::Peru;
			this->pictureBox3->Location = System::Drawing::Point(253, 242);
			this->pictureBox3->Name = L"pictureBox3";
			this->pictureBox3->Size = System::Drawing::Size(342, 213);
			this->pictureBox3->TabIndex = 48;
			this->pictureBox3->TabStop = false;
			// 
			// textBox22
			// 
			this->textBox22->Location = System::Drawing::Point(398, 283);
			this->textBox22->Name = L"textBox22";
			this->textBox22->Size = System::Drawing::Size(86, 20);
			this->textBox22->TabIndex = 49;
			this->textBox22->TextChanged += gcnew System::EventHandler(this, &Form1::textBox22_TextChanged);
			// 
			// textBox23
			// 
			this->textBox23->Location = System::Drawing::Point(499, 282);
			this->textBox23->Name = L"textBox23";
			this->textBox23->Size = System::Drawing::Size(86, 20);
			this->textBox23->TabIndex = 50;
			this->textBox23->TextChanged += gcnew System::EventHandler(this, &Form1::textBox23_TextChanged);
			// 
			// textBox24
			// 
			this->textBox24->Location = System::Drawing::Point(398, 305);
			this->textBox24->Name = L"textBox24";
			this->textBox24->Size = System::Drawing::Size(86, 20);
			this->textBox24->TabIndex = 51;
			this->textBox24->TextChanged += gcnew System::EventHandler(this, &Form1::textBox24_TextChanged);
			// 
			// textBox25
			// 
			this->textBox25->Location = System::Drawing::Point(499, 304);
			this->textBox25->Name = L"textBox25";
			this->textBox25->Size = System::Drawing::Size(86, 20);
			this->textBox25->TabIndex = 52;
			this->textBox25->TextChanged += gcnew System::EventHandler(this, &Form1::textBox25_TextChanged);
			// 
			// textBox26
			// 
			this->textBox26->Location = System::Drawing::Point(397, 328);
			this->textBox26->Name = L"textBox26";
			this->textBox26->Size = System::Drawing::Size(86, 20);
			this->textBox26->TabIndex = 53;
			this->textBox26->TextChanged += gcnew System::EventHandler(this, &Form1::textBox26_TextChanged);
			// 
			// textBox27
			// 
			this->textBox27->Location = System::Drawing::Point(499, 326);
			this->textBox27->Name = L"textBox27";
			this->textBox27->Size = System::Drawing::Size(86, 20);
			this->textBox27->TabIndex = 54;
			this->textBox27->TextChanged += gcnew System::EventHandler(this, &Form1::textBox27_TextChanged);
			// 
			// textBox28
			// 
			this->textBox28->Location = System::Drawing::Point(397, 349);
			this->textBox28->Name = L"textBox28";
			this->textBox28->Size = System::Drawing::Size(86, 20);
			this->textBox28->TabIndex = 55;
			this->textBox28->TextChanged += gcnew System::EventHandler(this, &Form1::textBox28_TextChanged);
			// 
			// textBox29
			// 
			this->textBox29->Location = System::Drawing::Point(499, 348);
			this->textBox29->Name = L"textBox29";
			this->textBox29->Size = System::Drawing::Size(86, 20);
			this->textBox29->TabIndex = 56;
			this->textBox29->TextChanged += gcnew System::EventHandler(this, &Form1::textBox29_TextChanged);
			// 
			// textBox30
			// 
			this->textBox30->Location = System::Drawing::Point(397, 371);
			this->textBox30->Name = L"textBox30";
			this->textBox30->Size = System::Drawing::Size(86, 20);
			this->textBox30->TabIndex = 57;
			this->textBox30->TextChanged += gcnew System::EventHandler(this, &Form1::textBox30_TextChanged);
			// 
			// textBox31
			// 
			this->textBox31->Location = System::Drawing::Point(500, 370);
			this->textBox31->Name = L"textBox31";
			this->textBox31->Size = System::Drawing::Size(86, 20);
			this->textBox31->TabIndex = 58;
			this->textBox31->TextChanged += gcnew System::EventHandler(this, &Form1::textBox31_TextChanged);
			// 
			// textBox32
			// 
			this->textBox32->Location = System::Drawing::Point(397, 393);
			this->textBox32->Name = L"textBox32";
			this->textBox32->Size = System::Drawing::Size(86, 20);
			this->textBox32->TabIndex = 59;
			this->textBox32->TextChanged += gcnew System::EventHandler(this, &Form1::textBox32_TextChanged);
			// 
			// textBox33
			// 
			this->textBox33->Location = System::Drawing::Point(500, 393);
			this->textBox33->Name = L"textBox33";
			this->textBox33->Size = System::Drawing::Size(86, 20);
			this->textBox33->TabIndex = 60;
			this->textBox33->TextChanged += gcnew System::EventHandler(this, &Form1::textBox33_TextChanged);
			// 
			// textBox34
			// 
			this->textBox34->Location = System::Drawing::Point(398, 415);
			this->textBox34->Name = L"textBox34";
			this->textBox34->Size = System::Drawing::Size(86, 20);
			this->textBox34->TabIndex = 61;
			this->textBox34->TextChanged += gcnew System::EventHandler(this, &Form1::textBox34_TextChanged);
			// 
			// textBox35
			// 
			this->textBox35->Location = System::Drawing::Point(500, 415);
			this->textBox35->Name = L"textBox35";
			this->textBox35->Size = System::Drawing::Size(86, 20);
			this->textBox35->TabIndex = 62;
			this->textBox35->TextChanged += gcnew System::EventHandler(this, &Form1::textBox35_TextChanged);
			// 
			// pictureBox4
			// 
			this->pictureBox4->BackColor = System::Drawing::Color::DarkKhaki;
			this->pictureBox4->Location = System::Drawing::Point(592, 242);
			this->pictureBox4->Name = L"pictureBox4";
			this->pictureBox4->Size = System::Drawing::Size(114, 213);
			this->pictureBox4->TabIndex = 63;
			this->pictureBox4->TabStop = false;
			// 
			// label14
			// 
			this->label14->AutoSize = true;
			this->label14->BackColor = System::Drawing::Color::DarkKhaki;
			this->label14->Location = System::Drawing::Point(616, 246);
			this->label14->Name = L"label14";
			this->label14->Size = System::Drawing::Size(73, 13);
			this->label14->TabIndex = 64;
			this->label14->Text = L"read delay (D)";
			// 
			// textBox36
			// 
			this->textBox36->Location = System::Drawing::Point(628, 263);
			this->textBox36->Name = L"textBox36";
			this->textBox36->Size = System::Drawing::Size(45, 20);
			this->textBox36->TabIndex = 65;
			// 
			// textBox37
			// 
			this->textBox37->Location = System::Drawing::Point(628, 284);
			this->textBox37->Name = L"textBox37";
			this->textBox37->Size = System::Drawing::Size(45, 20);
			this->textBox37->TabIndex = 66;
			// 
			// textBox38
			// 
			this->textBox38->Location = System::Drawing::Point(628, 305);
			this->textBox38->Name = L"textBox38";
			this->textBox38->Size = System::Drawing::Size(45, 20);
			this->textBox38->TabIndex = 67;
			// 
			// textBox39
			// 
			this->textBox39->Location = System::Drawing::Point(628, 326);
			this->textBox39->Name = L"textBox39";
			this->textBox39->Size = System::Drawing::Size(45, 20);
			this->textBox39->TabIndex = 68;
			// 
			// textBox40
			// 
			this->textBox40->Location = System::Drawing::Point(628, 348);
			this->textBox40->Name = L"textBox40";
			this->textBox40->Size = System::Drawing::Size(45, 20);
			this->textBox40->TabIndex = 69;
			// 
			// textBox41
			// 
			this->textBox41->Location = System::Drawing::Point(628, 369);
			this->textBox41->Name = L"textBox41";
			this->textBox41->Size = System::Drawing::Size(45, 20);
			this->textBox41->TabIndex = 70;
			// 
			// textBox42
			// 
			this->textBox42->Location = System::Drawing::Point(628, 391);
			this->textBox42->Name = L"textBox42";
			this->textBox42->Size = System::Drawing::Size(45, 20);
			this->textBox42->TabIndex = 71;
			// 
			// textBox43
			// 
			this->textBox43->Location = System::Drawing::Point(628, 412);
			this->textBox43->Name = L"textBox43";
			this->textBox43->Size = System::Drawing::Size(45, 20);
			this->textBox43->TabIndex = 72;
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(383, 73);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(51, 45);
			this->button1->TabIndex = 73;
			this->button1->Text = L"LOG Data";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Form1::button1_Click);
			// 
			// textBox44
			// 
			this->textBox44->Location = System::Drawing::Point(603, 71);
			this->textBox44->Name = L"textBox44";
			this->textBox44->Size = System::Drawing::Size(86, 20);
			this->textBox44->TabIndex = 74;
			this->textBox44->TextChanged += gcnew System::EventHandler(this, &Form1::textBox44_TextChanged);
			// 
			// label15
			// 
			this->label15->AutoSize = true;
			this->label15->Location = System::Drawing::Point(616, 55);
			this->label15->Name = L"label15";
			this->label15->Size = System::Drawing::Size(59, 13);
			this->label15->TabIndex = 75;
			this->label15->Text = L"Current Titr";
			// 
			// button6
			// 
			this->button6->Location = System::Drawing::Point(440, 75);
			this->button6->Name = L"button6";
			this->button6->Size = System::Drawing::Size(60, 42);
			this->button6->TabIndex = 76;
			this->button6->Text = L"Autoset";
			this->button6->UseVisualStyleBackColor = true;
			this->button6->Click += gcnew System::EventHandler(this, &Form1::button6_Click);
			// 
			// tabledataBindingSource
			// 
			this->tabledataBindingSource->DataSource = table_data::typeid;
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::Color::LightSeaGreen;
			this->ClientSize = System::Drawing::Size(823, 602);
			this->Controls->Add(this->button6);
			this->Controls->Add(this->label15);
			this->Controls->Add(this->textBox44);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->textBox43);
			this->Controls->Add(this->textBox42);
			this->Controls->Add(this->textBox41);
			this->Controls->Add(this->textBox40);
			this->Controls->Add(this->textBox39);
			this->Controls->Add(this->textBox38);
			this->Controls->Add(this->textBox37);
			this->Controls->Add(this->textBox36);
			this->Controls->Add(this->label14);
			this->Controls->Add(this->pictureBox4);
			this->Controls->Add(this->textBox35);
			this->Controls->Add(this->textBox34);
			this->Controls->Add(this->textBox33);
			this->Controls->Add(this->textBox32);
			this->Controls->Add(this->textBox31);
			this->Controls->Add(this->textBox30);
			this->Controls->Add(this->textBox29);
			this->Controls->Add(this->textBox28);
			this->Controls->Add(this->textBox27);
			this->Controls->Add(this->textBox26);
			this->Controls->Add(this->textBox25);
			this->Controls->Add(this->textBox24);
			this->Controls->Add(this->textBox23);
			this->Controls->Add(this->textBox22);
			this->Controls->Add(this->label13);
			this->Controls->Add(this->label12);
			this->Controls->Add(this->label11);
			this->Controls->Add(this->label10);
			this->Controls->Add(this->label9);
			this->Controls->Add(this->label8);
			this->Controls->Add(this->label7);
			this->Controls->Add(this->label6);
			this->Controls->Add(this->textBox21);
			this->Controls->Add(this->textBox20);
			this->Controls->Add(this->textBox19);
			this->Controls->Add(this->textBox18);
			this->Controls->Add(this->textBox17);
			this->Controls->Add(this->textBox16);
			this->Controls->Add(this->textBox15);
			this->Controls->Add(this->textBox14);
			this->Controls->Add(this->textBox13);
			this->Controls->Add(this->textBox12);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->pictureBox1);
			this->Controls->Add(this->button5);
			this->Controls->Add(this->textBox11);
			this->Controls->Add(this->labebuff7);
			this->Controls->Add(this->textBox10);
			this->Controls->Add(this->labebuff5);
			this->Controls->Add(this->textBox9);
			this->Controls->Add(this->labebuff4);
			this->Controls->Add(this->textBox8);
			this->Controls->Add(this->labebuff3);
			this->Controls->Add(this->button4);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->textBox7);
			this->Controls->Add(this->labebuff8);
			this->Controls->Add(this->textBox6);
			this->Controls->Add(this->labebuff2);
			this->Controls->Add(this->textBox5);
			this->Controls->Add(this->labebuff1);
			this->Controls->Add(this->textBox4);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->textBox3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->textBox2);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->pictureBox2);
			this->Controls->Add(this->pictureBox3);
			this->Name = L"Form1";
			this->Text = L"Buffer Based Dataflow ( BY : SAMBHAV VERMAN)";
			this->Load += gcnew System::EventHandler(this, &Form1::Form1_Load);
			this->Activated += gcnew System::EventHandler(this, &Form1::Form1_Load);
			this->Validated += gcnew System::EventHandler(this, &Form1::button2_Click);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->pictureBox4))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->tabledataBindingSource))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion


	private: System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e) {
				

			 }
			 

	private: System::Void textBox1_TextChanged(System::Object^  sender, System::EventArgs^  e) {
				 if(textBox1->Text!= "")
				 {
				
					 f1 = System::Convert::ToInt32(textBox1->Text);
				
				 }
			 }
private: System::Void textBox2_TextChanged(System::Object^  sender, System::EventArgs^  e) {
			 if(textBox2->Text!= "")
			 {
					 f2 = System::Convert::ToInt32(textBox2->Text);
				
			 }
		 }
private: System::Void textBox3_TextChanged(System::Object^  sender, System::EventArgs^  e) {

			f3 = System::Convert::ToInt32(textBox3->Text);
		 }
private: System::Void textBox4_TextChanged(System::Object^  sender, System::EventArgs^  e) {

			f4 = System::Convert::ToInt32(textBox4->Text);
		 }
private: System::Void textBox5_TextChanged(System::Object^  sender, System::EventArgs^  e) {

		//	 fclk_gen1 = System::Convert::ToInt32(textBox5->Text);
		 }
private: System::Void textBox6_TextChanged(System::Object^  sender, System::EventArgs^  e) {

			//  fclk_gen2 = System::Convert::ToInt32(textBox6->Text);
		 }
private: System::Void textBox7_TextChanged(System::Object^  sender, System::EventArgs^  e) {

			  Req_Titr = System::Convert::ToInt32(textBox7->Text);
		 }

private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
		

			 frm->Visible = false;
			 
		 if (frm->IsDisposed)
		 {
		return;
		 }
		 else
			 frm->Show(this);
		 textBox36->Text = System::Convert::ToString(buff1->D);
		 textBox37->Text = System::Convert::ToString(buff2->D);
		 textBox38->Text = System::Convert::ToString(buff3->D);
		 textBox39->Text = System::Convert::ToString(buff4->D);
		 textBox40->Text = System::Convert::ToString(buff5->D);
		 textBox41->Text = System::Convert::ToString(buff6->D);
		 textBox42->Text = System::Convert::ToString(buff7->D);
		 textBox43->Text = System::Convert::ToString(buff8->D);
	
		 }
private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {
	
		 fclose(fptr);
		 fclose(fptr2);	
			 frm->Close();
			 
		 }
private : int showandhide(int key){

		 return key;
		  }
private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e) {

			 exit(0);
		 }
private: System::Void textBox8_TextChanged(System::Object^  sender, System::EventArgs^  e) {

			 M1 = System::Convert::ToInt32(textBox8->Text);
		 }
private: System::Void textBox9_TextChanged(System::Object^  sender, System::EventArgs^  e) {
			 L1 = System::Convert::ToInt32(textBox9->Text);
			
			
			//assign same latency to all processing elements for now
		 }

//private: System::Void textBox10_TextChanged(System::Object^  sender, System::EventArgs^  e) {
//
//			 nw = System::Convert::ToInt32(textBox10->Text);
//
//		 }
private: System::Void textBox11_TextChanged(System::Object^  sender, System::EventArgs^  e) {

			 nr1 = System::Convert::ToInt32(textBox11->Text);
		 }
	
private: System::Void button5_Click(System::Object^  sender, System::EventArgs^  e) {

			if (frm->IsDisposed)
				return;
			else
				RecreateHandle();
			  frm->Invalidate();
			 frm->Update();
			 frm->Show();
			
			 
			 
			
		 }
private: System::Void textBox10_TextChanged_1(System::Object^  sender, System::EventArgs^  e) {
			  nw1 = System::Convert::ToInt32(textBox10->Text);

		 }
private: System::Void label1_Click(System::Object^  sender, System::EventArgs^  e) {
		 }
private: System::Void textBox12_TextChanged(System::Object^  sender, System::EventArgs^  e) {

			 M2 = System::Convert::ToInt32(textBox12->Text);
		 }
private: System::Void textBox14_TextChanged(System::Object^  sender, System::EventArgs^  e) {
			 M3 = System::Convert::ToInt32(textBox14->Text);
		 }
private: System::Void textBox13_TextChanged(System::Object^  sender, System::EventArgs^  e) {
			 M4 = System::Convert::ToInt32(textBox13->Text);
		 }
private: System::Void textBox16_TextChanged(System::Object^  sender, System::EventArgs^  e) {
			
			 L2 = System::Convert::ToInt32(textBox16->Text);
		 }
private: System::Void textBox15_TextChanged(System::Object^  sender, System::EventArgs^  e) {

			  L3 = System::Convert::ToInt32(textBox15->Text);
		 }
private: System::Void textBox17_TextChanged(System::Object^  sender, System::EventArgs^  e) {

			  L4 = System::Convert::ToInt32(textBox17->Text);
		 }
private: System::Void textBox18_TextChanged(System::Object^  sender, System::EventArgs^  e) {
			 M5 = System::Convert::ToInt32(textBox18->Text);
		 }
private: System::Void textBox19_TextChanged(System::Object^  sender, System::EventArgs^  e) {
			 M6 = System::Convert::ToInt32(textBox19->Text);
		 }
private: System::Void textBox20_TextChanged(System::Object^  sender, System::EventArgs^  e) {
			 M7 = System::Convert::ToInt32(textBox20->Text);
		 }
private: System::Void textBox21_TextChanged(System::Object^  sender, System::EventArgs^  e) {

			  M8 = System::Convert::ToInt32(textBox21->Text);
		 }
private: System::Void textBox23_TextChanged(System::Object^  sender, System::EventArgs^  e) {
			 nr2 = System::Convert::ToInt32(textBox23->Text);
		 }
private: System::Void textBox22_TextChanged(System::Object^  sender, System::EventArgs^  e) {
			 nw2 = System::Convert::ToInt32(textBox22->Text);

		 }
private: System::Void textBox24_TextChanged(System::Object^  sender, System::EventArgs^  e) {

			  nw3 = System::Convert::ToInt32(textBox24->Text);
		 }
private: System::Void textBox25_TextChanged(System::Object^  sender, System::EventArgs^  e) {

			  nr3 = System::Convert::ToInt32(textBox25->Text);
		 }
private: System::Void textBox26_TextChanged(System::Object^  sender, System::EventArgs^  e) {

			  nw4 = System::Convert::ToInt32(textBox26->Text);
		 }
private: System::Void textBox27_TextChanged(System::Object^  sender, System::EventArgs^  e) {
			  nr4 = System::Convert::ToInt32(textBox27->Text);
		 }
private: System::Void textBox28_TextChanged(System::Object^  sender, System::EventArgs^  e) {

			  nw5 = System::Convert::ToInt32(textBox28->Text);
		 }
private: System::Void textBox29_TextChanged(System::Object^  sender, System::EventArgs^  e) {

			  nr5 = System::Convert::ToInt32(textBox29->Text);
		 }
private: System::Void textBox30_TextChanged(System::Object^  sender, System::EventArgs^  e) {

			  nw6 = System::Convert::ToInt32(textBox30->Text);
		 }
private: System::Void textBox31_TextChanged(System::Object^  sender, System::EventArgs^  e) {

			  nr6 = System::Convert::ToInt32(textBox31->Text);
		 }
private: System::Void textBox32_TextChanged(System::Object^  sender, System::EventArgs^  e) {
			 
			 nw7 = System::Convert::ToInt32(textBox32->Text);
		 }
private: System::Void textBox33_TextChanged(System::Object^  sender, System::EventArgs^  e) {

			  nr7 = System::Convert::ToInt32(textBox33->Text);
		 }
private: System::Void textBox34_TextChanged(System::Object^  sender, System::EventArgs^  e) {

			  nw8 = System::Convert::ToInt32(textBox34->Text);
		 }
private: System::Void textBox35_TextChanged(System::Object^  sender, System::EventArgs^  e) {

			  nr8 = System::Convert::ToInt32(textBox35->Text);
		 }

		
private: System::Void textBox36_TextChanged(System::Object^  sender, System::EventArgs^  e) {
		 }
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e)
		 
		 {

		LPCWSTR open_L1 = L"open";
		LPCWSTR path_L1 = L"C:\\BBDFlog2.txt";
		ShellExecuteW(NULL, open_L1, path_L1,NULL,NULL,SW_SHOWNORMAL);


		LPCWSTR open_L2 = L"open";
		LPCWSTR path_L2 = L"C:\\memdata.txt";
		ShellExecuteW(NULL, open_L2, path_L2,NULL,NULL,SW_SHOWNORMAL);
		

		 }
private: System::Void textBox44_TextChanged(System::Object^  sender, System::EventArgs^  e) {


		 }
private: System::Void button6_Click(System::Object^  sender, System::EventArgs^  e) {
		
			  textBox1->Text = System::Convert::ToString(2);//f1
			  textBox2->Text = System::Convert::ToString(4);//f2
			  textBox3->Text = System::Convert::ToString(8);//f3
			  textBox4->Text = System::Convert::ToString(1);//f4

			  textBox9->Text = System::Convert::ToString(1);//L1
			  textBox16->Text = System::Convert::ToString(1);//L2
			  textBox15->Text = System::Convert::ToString(1);//L3
			  textBox17->Text = System::Convert::ToString(1);//L4

			  textBox8->Text = System::Convert::ToString(8);//M1
			  textBox12->Text = System::Convert::ToString(8);//M2
			  textBox14->Text = System::Convert::ToString(8);//M3
			  textBox13->Text = System::Convert::ToString(8);//M4
			  textBox18->Text = System::Convert::ToString(8);//M5
			  textBox19->Text = System::Convert::ToString(8);//M6
			  textBox20->Text = System::Convert::ToString(8);//M7
			  textBox21->Text = System::Convert::ToString(8);//M8

			  textBox10->Text = System::Convert::ToString(1);//nw1
			  textBox11->Text = System::Convert::ToString(1);//nr1
			  textBox22->Text = System::Convert::ToString(1);//nw2
			  textBox23->Text = System::Convert::ToString(1);//nr2
			  textBox24->Text = System::Convert::ToString(1);//nw3
			  textBox25->Text = System::Convert::ToString(1);//nr3
			  textBox26->Text = System::Convert::ToString(1);//nw4
			  textBox27->Text = System::Convert::ToString(1);//nr4
			  textBox28->Text = System::Convert::ToString(1);//nw5
			  textBox29->Text = System::Convert::ToString(1);//nr5
			  textBox30->Text = System::Convert::ToString(1);//nw6
			  textBox31->Text = System::Convert::ToString(1);//nr6
			  textBox32->Text = System::Convert::ToString(1);//nw7
			  textBox33->Text = System::Convert::ToString(1);//nr7
			  textBox34->Text = System::Convert::ToString(1);//nw8
			  textBox35->Text = System::Convert::ToString(1);//nr8

			  textBox7->Text = System::Convert::ToString(8);//Req_Titr


				

		 }
};
}

